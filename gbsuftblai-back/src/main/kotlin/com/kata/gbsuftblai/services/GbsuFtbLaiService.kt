package com.kata.gbsuftblai.services

import org.springframework.stereotype.Component
import kotlin.text.StringBuilder

@Component
class GbsuFtbLaiService {

    val GBSU: String = "Gbsu";
    val FTB: String = "Ftb";
    val LAI: String = "Lai";

    fun convertNumber(inputNumber: Int): String {

        val result: StringBuilder = StringBuilder();

        checkDivisibility(inputNumber, result);
        checkDigits(inputNumber, result);

        if(result.isEmpty()) return inputNumber.toString();
        return result.toString();
    }

    fun checkDivisibility(inputNumber: Int, sb : StringBuilder) {
        if(inputNumber%3 == 0) sb.append(GBSU);
        if(inputNumber%5 == 0) sb.append(FTB);
    }

    fun checkDigits(inputNumber: Int, sb: StringBuilder){
        for(c:Char in inputNumber.toString().toCharArray()){
            if(c == '3') {
                sb.append(GBSU);
            }
            else if(c == '5') {
                sb.append(FTB);
            }
            else if(c == '7') {
                sb.append(LAI);
            }
        }
    }

}