package com.kata.gbsuftblai.controllers

import com.kata.gbsuftblai.services.GbsuFtbLaiService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GbsuFtbLaiControllerTests{

    @Mock
    lateinit var  gbsuFtbLaiService : GbsuFtbLaiService;

    lateinit var gbsuFtbLaiController : GbsuFtbLaiController;

    @BeforeAll
    fun init(){
        gbsuFtbLaiController = GbsuFtbLaiController(gbsuFtbLaiService);
        Mockito.`when`(gbsuFtbLaiService.convertNumber(1)).thenReturn("1");
        Mockito.`when`(gbsuFtbLaiService.convertNumber(3)).thenReturn("GbsuGbsu");
        Mockito.`when`(gbsuFtbLaiService.convertNumber(5)).thenReturn("FtbFtb");
        Mockito.`when`(gbsuFtbLaiService.convertNumber(7)).thenReturn("Lai");
        Mockito.`when`(gbsuFtbLaiService.convertNumber(9)).thenReturn("Gbsu");
        Mockito.`when`(gbsuFtbLaiService.convertNumber(51)).thenReturn("GbsuFtb");
        Mockito.`when`(gbsuFtbLaiService.convertNumber(53)).thenReturn("FtbGbsu");
        Mockito.`when`(gbsuFtbLaiService.convertNumber(33)).thenReturn("GbsuGbsuGbsu");
        Mockito.`when`(gbsuFtbLaiService.convertNumber(27)).thenReturn("GbsuLai");
        Mockito.`when`(gbsuFtbLaiService.convertNumber(15)).thenReturn("GbsuFtbFtb");
    }

    @Test
    fun testConvertNumber(){
        Assertions.assertEquals(GbsuFtbLaiController.ResultDto("1"), gbsuFtbLaiController.convertNumber(1));
        Assertions.assertEquals(GbsuFtbLaiController.ResultDto("GbsuGbsu"), gbsuFtbLaiController.convertNumber(3));
        Assertions.assertEquals(GbsuFtbLaiController.ResultDto("FtbFtb"), gbsuFtbLaiController.convertNumber(5));
        Assertions.assertEquals(GbsuFtbLaiController.ResultDto("Lai"), gbsuFtbLaiController.convertNumber(7));
        Assertions.assertEquals(GbsuFtbLaiController.ResultDto("Gbsu"), gbsuFtbLaiController.convertNumber(9));
        Assertions.assertEquals(GbsuFtbLaiController.ResultDto("GbsuFtb"), gbsuFtbLaiController.convertNumber(51));
        Assertions.assertEquals(GbsuFtbLaiController.ResultDto("FtbGbsu"), gbsuFtbLaiController.convertNumber(53));
        Assertions.assertEquals(GbsuFtbLaiController.ResultDto("GbsuGbsuGbsu"), gbsuFtbLaiController.convertNumber(33));
        Assertions.assertEquals(GbsuFtbLaiController.ResultDto("GbsuLai"), gbsuFtbLaiController.convertNumber(27));
        Assertions.assertEquals(GbsuFtbLaiController.ResultDto("GbsuFtbFtb"), gbsuFtbLaiController.convertNumber(15));
    }
}