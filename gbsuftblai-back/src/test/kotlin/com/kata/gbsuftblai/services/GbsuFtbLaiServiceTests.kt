package com.kata.gbsuftblai.services;

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GbsuFtbLaiServiceTests {

    lateinit var gbsuFtbLaiService :  GbsuFtbLaiService;

    @BeforeAll
    fun init(){
        gbsuFtbLaiService = GbsuFtbLaiService()
    }

    @Test
    fun testConvertNumber(){
        Assertions.assertEquals("1", gbsuFtbLaiService.convertNumber(1));
        Assertions.assertEquals("GbsuGbsu", gbsuFtbLaiService.convertNumber(3));
        Assertions.assertEquals("FtbFtb", gbsuFtbLaiService.convertNumber(5));
        Assertions.assertEquals("Lai", gbsuFtbLaiService.convertNumber(7));
        Assertions.assertEquals("Gbsu", gbsuFtbLaiService.convertNumber(9));
        Assertions.assertEquals("GbsuFtb", gbsuFtbLaiService.convertNumber(51));
        Assertions.assertEquals("FtbGbsu", gbsuFtbLaiService.convertNumber(53));
        Assertions.assertEquals("GbsuGbsuGbsu", gbsuFtbLaiService.convertNumber(33));
        Assertions.assertEquals("GbsuLai", gbsuFtbLaiService.convertNumber(27));
        Assertions.assertEquals("GbsuFtbFtb", gbsuFtbLaiService.convertNumber(15));
    }
}
